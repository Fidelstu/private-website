var express = require('express');
var app = express();
var Setting = require('./_service/ObjectFactory/setting.json');

var routerProfile = require('./_service/Router/router-profile.js');
var routerIndex= require('./_service/Router/router-index.js');
/*
var StaticServer = require('static-server');
const server = new StaticServer({
	rootPath: Setting.root,
	port: Setting.port
});
*/

//app.configure(function(){
  app.use('/Pages', express.static(__dirname + '/Pages'));
  app.use(express.static(__dirname + '/_ui'));
//});

app.use('/',routerIndex);
app.use('/profile',routerProfile);

var port = process.env.PORT || Setting.port;
//app.listen(Setting.port);
var server = app.listen(port , function () {
    var host = server.address().address;
    var porta = server.address().port;
    var info = host.length > 4 ? host:'localhost';
    console.log('running at http://' + info + ':' + porta)
});
/*
server.start(function () {
	console.log('Server started on port ' + port);
});
*/

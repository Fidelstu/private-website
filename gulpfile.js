var gulp = require('gulp');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var babel = require('gulp-babel');
var runSequence = require('run-sequence');

// File paths
var JS_PATH = '_dev/Controllers/*.js';
var JS_DEST = '_ui/Controllers';
var SV_PATH = '_dev/Services/*.js';
//var SV_DEST = '_ui/Services';
var VD_PATH = '_dev/Vendors/*.js';
var UI_DEST = '_ui';

//var JS_PATH = '_ui/Controllers/*.js';
//var JS_DEST = '_ui/Controllers';
var STYLE_PATH = '_dev/Styles/*.css';
var STYLE_DEST = '_ui/Styles';

// Styles
 gulp.task('styles', function () {
 	console.log('starting styles task');
 	return gulp.src(['_dev/Styles/reset.css',STYLE_PATH])
 		.pipe(plumber(function (err) {
 			console.log('Styles Task Error');
 			console.log(err);
 			this.emit('end');
 		}))
 		.pipe(sourcemaps.init())
 		.pipe(autoprefixer())
 		.pipe(concat('styles.css'))
 		//.pipe(minifyCss())
 		.pipe(sourcemaps.write())
 		.pipe(gulp.dest(STYLE_DEST))
 		.pipe(livereload());
 });

 gulp.task('styles-prod', function () {
  console.log('starting styles task');
  return gulp.src(['_dev/Styles/reset.css',STYLE_PATH])
    .pipe(plumber(function (err) {
      console.log('Styles Task Error');
      console.log(err);
      this.emit('end');
    }))
    //.pipe(sourcemaps.init())
 		.pipe(autoprefixer())
 		.pipe(concat('styles.css'))
 		.pipe(minifyCss())
 		//.pipe(sourcemaps.write())
 		.pipe(gulp.dest(STYLE_DEST))
 		.pipe(livereload());
 });

// Scripts
gulp.task('vd', function () {
	console.log('starting scripts task');

	return gulp.src(VD_PATH)
		.pipe(plumber(function (err) {
			console.log('Scripts Task Error');
			console.log(err);
			this.emit('end');
		}))
		//.pipe(sourcemaps.init())
		.pipe(uglify())
		//.pipe(sourcemaps.write())
    .pipe(concat('vendors.js'))
		.pipe(gulp.dest(UI_DEST))
		.pipe(livereload());
});

gulp.task('sv', function () {
	console.log('starting scripts task');

	return gulp.src(SV_PATH)
		.pipe(plumber(function (err) {
			console.log('Scripts Task Error');
			console.log(err);
			this.emit('end');
		}))
		//.pipe(sourcemaps.init())
    .pipe(babel())
		//.pipe(uglify())
		//.pipe(sourcemaps.write())
    .pipe(concat('services.js'))
		.pipe(gulp.dest(UI_DEST))
		.pipe(livereload());
});

gulp.task('sv-prod', function () {
	console.log('starting scripts task');

	return gulp.src(SV_PATH)
		.pipe(plumber(function (err) {
			console.log('Scripts Task Error');
			console.log(err);
			this.emit('end');
		}))
		//.pipe(sourcemaps.init())
    .pipe(babel())
		.pipe(uglify())
		//.pipe(sourcemaps.write())
    .pipe(concat('services.js'))
		.pipe(gulp.dest(UI_DEST))
		.pipe(livereload());
});

gulp.task('ctrl', function () {
	console.log('starting scripts task');

	return gulp.src(JS_PATH)
		.pipe(plumber(function (err) {
			console.log('Scripts Task Error');
			console.log(err);
			this.emit('end');
		}))
    //.pipe(sourcemaps.init())
    .pipe(babel())
		//.pipe(uglify())
		//.pipe(sourcemaps.write())
		.pipe(gulp.dest(JS_DEST))
		.pipe(livereload());
});

gulp.task('ctrl-prod', function () {
	console.log('starting scripts task');

	return gulp.src(JS_PATH)
		.pipe(plumber(function (err) {
			console.log('Scripts Task Error');
			console.log(err);
			this.emit('end');
		}))
    //.pipe(sourcemaps.init())
    .pipe(babel())
		.pipe(uglify())
		//.pipe(sourcemaps.write())
		.pipe(gulp.dest(JS_DEST))
		.pipe(livereload());
});

gulp.task('watch', function () {
	console.log('Starting watch task');
	require('./server.js');
	livereload.listen();
  gulp.watch(VD_PATH, ['vd']);
  gulp.watch(SV_PATH, ['sv']);
	gulp.watch(JS_PATH, ['ctrl']);
	gulp.watch(STYLE_PATH, ['styles']);
});

gulp.task('watch-prod', function () {
	console.log('Starting watch task');
	require('./server.js');
	livereload.listen();
  gulp.watch(VD_PATH, ['vd']);
  gulp.watch(SV_PATH, ['sv-prod']);
	gulp.watch(JS_PATH, ['ctrl-prod']);
	gulp.watch(STYLE_PATH, ['styles-prod']);
});

// start app
gulp.task('start-dev', function() {
  runSequence('vd','sv','ctrl','styles','watch' );
});

gulp.task('start-prod', function() {
  runSequence('vd','sv-prod','ctrl-prod','styles-prod','watch-prod');
});

// default
gulp.task('default', ['styles-prod','vd','sv-prod','ctrl-prod','watch-prod']);

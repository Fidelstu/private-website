;(function($f, w, h, i, fly){
  var imFly = function(){
    this.Application = "fly";
    this.data = {};
  }
  h = imFly.prototype.h = {};
  i = imFly.prototype.to = {};

  fly = new imFly();
  function add(key,fill,bl){
    return addToFly(key,fill,bl);
  }

  function addToFly(key,fill,bl){
    var type = _type(fill), front = _isFront(key);

    if(front && !bl){
      _fillFront(key,fill,bl);
    }
    else if(_type(key) == "Object" && !fill && !bl){
      _fillObj(null,key);
    }
    else if(type == "Object"){
      _fillObj(key,fill,bl);
    }
    else if(type == "Function"){
      _fillFn(key,fill);
    }else{
      _fillData(key,fill,bl)
    }

    return this;
  }

  function _fillObj(key,fill,bl){

    if(!bl){
      for(var p in fill){
        var type = _type(fill[p]), front = _isFront(p);
        if(front){
          _fillFront(p,fill[p]);
        }else if(type == "Object"){
          _fillObj(p,fill[p],bl);
        }else if(type == "Function"){
          _fillFn(p,fill[p]);
        }
        else{
          _fillData(p,fill[p]);
        }
      }
    }else{
      _fillData(key,fill);
    }
  }

  function _type(fill){
    var type = Object.prototype.toString.call(fill);
    return type.split(" ")[1].slice(0,-1);
  }

  function _isFront(key){
    return fly.hasOwnProperty(key);
  }

  function _fillFront(key,fill,bl){
    var type = _type(fill);
    if(key != "data" && key != "fn" && type != "Function"){
      fly[key] = fill;
    }
  }

  function _fillFn(key,fill){
    if(_type(fill) == "Function"){
      //fly["fn"][key] = fill;
      i[key] = fill;
    }else{
      fly["data"][key] = fill;
    }
  }

  function _fillData(key,fill,bl){

    if(!bl){
      fly["data"][key] = fill;
    }else{
      fly[key] = fill;
    }
  }

  w["fly"] = fly;
  w["$f"] = add;

})("$f", window)

/*
debuggerrr
$f("Application","sfsdfsdf");
$f({
	Application : "Rloaded"
	,qw : "fgfdggd"
	,fg : "dftt"
	,func : function(){return "asda"}
})
debugger
*/

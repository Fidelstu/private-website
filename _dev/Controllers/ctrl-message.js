var onloadCallback = function() {
      grecaptcha.render('capcay', {
        'sitekey' : '6LfY_EMUAAAAAD-1tj3MIW6YBkjgUiw9qNVR8Tez',
        'theme' : 'dark'
      });
    };
$.getScript("https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit");

$(function(){
  var urlp = "https://fidelstu.com/private-website/SendMessage";
  //var urlp = "http://service.fidelstu.com:4444/private-website/SendMessage";
  //var urlp = "https://localhost:5454/private-website/SendMessage";
  //var urlp = "http://localhost:4444/private-website/SendMessage";

  // Attach a submit handler to the form
  $( "#formMessage" ).submit(function( event ) {
    $(".magnifier").show();
    // Stop form from submitting normally
    event.preventDefault();

    // Get some values from elements on the page:
      var dForm = $(this).serializeArray().reduce(function(obj, item) {
          obj[item.name] = item.value;
          return obj;
      }, {});



    // Send the data using post
    $.post(urlp, dForm)
          .done(function( data ) {
            //return from end point response

            $(".messageInfo").addClass(data.status).html(data.message).show();
            console.log(data.status);
            setTimeout(function(){
              $(".messageInfo").removeClass(data.status).fadeOut( "slow" );
            },6000)

            $("#formMessage").each(function(){
                this.reset();
            });
            grecaptcha.reset();
          });

  });

})

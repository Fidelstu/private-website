$f("flyLoad",function(p,s){

  if ($("."+fly.data.load+"_page").length === 0){

    $(".magnifier").show();
      $.when( $.ajax( "Pages/" + p +".html" ), $(s) ).then(function(h, e, s){
          e.append(h[0]);
          $("."+fly.data.load+"_page").siblings().hide();
          if(fly.data.index == fly.data.load)$f("load","index");
          var condCtrl = $("."+fly.data.load+"_pages").length > 0 || $("."+fly.data.load + "_page > controller").length > 0;
          if (condCtrl){
            $.getScript("Controllers/ctrl-"+ fly.data.load +".js");
          }
      }).then(function(){
          $(".magnifier").fadeOut( "slow" ); //.hide();
        }
      )
  }else{
    $("."+fly.data.load+"_page").show();
    $("."+fly.data.load+"_page").siblings().hide();
  }
})

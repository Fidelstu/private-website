$f("qs",function(){
  var result = location.search.match(/[\?\&][^\?\&]+=[^\?\&]+/g);
  var qs = {};
  for(var i = 0; result && i < result.length; i++){
    var str = result[i].substring(1),
      keyStr = str.split('=')[0].toUpperCase(),
      valueStr = str.split('=')[1];
     qs[keyStr] = valueStr;
   }
   return qs;
})

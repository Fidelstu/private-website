$f("callService",function(urlStr,args,fnSuccess,fnError){
  function succes (data){
    fnSuccess.call(this,data)
  };
  function err (data){
    fnError.call(this,data)
  };
  var serviceObj = {
    url: urlStr
    , data: args
    , dataType: 'json'
    , success:succes
    , error:fnError? err : null
  };
  $.ajax(serviceObj);
})

/*
 function callService(urlStr,args,fnSuccess,fnError){
   function succes (data){
     fnSuccess.call(this,data)
   };
   function err (data){
     fnError.call(this,data)
   };
   var serviceObj = {
     url: urlStr
     , data: args
     , dataType: 'json'
     , success:succes
     , error:fnError? err : null
   };
   $.ajax(serviceObj);
 };

   function qsFn(){
     var result = location.search.match(/[\?\&][^\?\&]+=[^\?\&]+/g);
     var qs = {};
     for(var i = 0; result && i < result.length; i++){
       var str = result[i].substring(1),
         keyStr = str.split('=')[0].toUpperCase(),
         valueStr = str.split('=')[1];
        qs[keyStr] = valueStr;
      }
      return qs;
   };

   var fly = {
     apps : "private-website"
     ,index : "im"
     ,qs : qsFn()
   };
*/
//init start

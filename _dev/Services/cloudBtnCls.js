$f("cloudBtnCls", function(name){

  class cloudBtnCls{

    constructor(name) {
      //this.text = text;
      this.Name = name;
      this.prefix = name.substring(0,2).toLowerCase();
    }

    one(text){
        var selector = "."+this.prefix+"mc-one";
        if(fly["data"][this.Name+"Active"]["active"] != selector){
          $(selector).animate({'marginLeft' : "-=130px"},"slow");

          $(selector).animate({'top' : "+=70px"},"slow");
          $(selector).animate({'marginLeft' : "+=130px"},"slow");

          $(fly["data"][this.Name+"Active"]["active"]).animate({'top' : "-=70px"},"slow");

          $f(this.Name+"Active",{
              active: selector
            },1);

          fly.to.pageLoad(this.Name+"/"+text)

        }
    }
    two(text){

      var selector = "."+this.prefix+"mc-two";
      if( fly["data"][this.Name+"Active"]["active"] != selector){
        $(selector).animate({'marginLeft' : "-=90px"},"slow");

        $(selector).animate({'top' : "+=70px"},"slow");
        $(selector).animate({'marginLeft' : "+=90px"},"slow");

        $(fly["data"][this.Name+"Active"]["active"]).animate({'top' : "-=70px"},"slow");

        $f(this.Name+"Active",{
            active: selector
          },1);

        fly.to.pageLoad(this.Name+"/"+text)
      }
    }
    three(text){
      var selector = "."+this.prefix+"mc-three";
      if(fly["data"][this.Name+"Active"]["active"] != selector){
        $(selector).animate({'marginLeft' : "+=85px"},"slow");
        $(selector).animate({'top' : "+=70px"},"slow");
        $(selector).animate({'marginLeft' : "-=85px"},"slow");

          $(fly["data"][this.Name+"Active"]["active"]).animate({'top' : "-=70px"},"slow");


        $f(this.Name+"Active",{
            active: selector
          },1);


          fly.to.pageLoad(this.Name+"/"+text)
        }
    }
    four(text){
      var selector = "."+this.prefix+"mc-four";
      if(fly["data"][this.Name+"Active"]["active"] != selector){
        $(selector).animate({'marginLeft' : "+=130px"},"slow");
        $(selector).animate({'top' : "+=70px"},"slow");
        $(selector).animate({'marginLeft' : "-=130px"},"slow");
          $(fly["data"][this.Name+"Active"]["active"]).animate({'top' : "-=70px"},"slow");


        $f(this.Name+"Active",{
            active: selector
          },1);
        fly.to.pageLoad(this.Name+"/"+text)
      }
    }

  }

  return new cloudBtnCls(name);
})

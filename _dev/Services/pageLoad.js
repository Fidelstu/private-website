$f("pageLoad",function(page,selector){
  var p = page || fly.to.qs()["PAGE"] || fly.data.index;

  var ps = page && page.indexOf("/") != -1 ? page.split("/") : null;
  if(ps)ps.splice(ps.length-1,1,"pages");

  var s = page && page.indexOf("/") != -1? "." + ps.join("_").toLowerCase() : "";
  s = s || selector || ".index_pages";

  $f("load",p.split("/")[p.split("/").length-1]);
  var sc = fly.data.index == fly.data.load ? "index" : fly.data.load;

  fly.to.flyLoad(p,s);

});
